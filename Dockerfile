# Start with the Chainguard Go image
FROM cgr.dev/chainguard/go:latest as build-image

# Set the Node version
ARG NODE_VERSION=20.8.0

# Download and install Node.js
# Determine architecture
RUN ARCH="" && \
    case $(uname -m) in \
    x86_64) ARCH="x64" ;; \
    aarch64) ARCH="arm64" ;; \
    esac && \
    wget -O node.tar.xz https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-${ARCH}.tar.xz && \
    tar -xJf node.tar.xz -C /usr/local --strip-components=1 && \
    rm node.tar.xz

# Verify Node.js installation
RUN node -v && npm -v