// main.go
package main

import (
	"log"
	"os"
	"path/filepath"

	"github.com/yourusername/yourprojectname/pkg/generate"
)

func main() {
	outDir := "out"
	tf, err := generate.ReadTaskfile("Taskfile.yaml")
	if err != nil {
		log.Fatalf("Error reading Taskfile: %s", err)
	}

	gitlabDir := filepath.Join(outDir, ".gitlab")
	if err = os.MkdirAll(gitlabDir, 0755); err != nil {
		log.Fatalf("Failed to create directory for GitHub Actions: %s", err)
	}
	err = generate.GenerateGitLabCI(tf, filepath.Join(gitlabDir, ".gitlab-task-pipeline.yaml"))
	if err != nil {
		log.Fatalf("Error generating GitLab CI file: %s", err)
	}

	githubDir := filepath.Join(outDir, ".github")
	if err = os.MkdirAll(githubDir, 0755); err != nil {
		log.Fatalf("Failed to create directory for GitHub Actions: %s", err)
	}

	err = generate.GenerateGitHubWorkflow(tf, filepath.Join(githubDir, "task_workflow.yml"))
	if err != nil {
		log.Fatalf("Error generating GitHub Workflow file: %s", err)
	}

	log.Println("Files generated successfully!")
}
