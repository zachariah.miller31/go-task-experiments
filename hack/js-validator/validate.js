const fs = require('fs');
const https = require('https');
const yaml = require('js-yaml');
const Ajv = require('ajv');

// Load YAML from the provided file path
function loadYAML(filePath) {
    const file = fs.readFileSync(filePath, 'utf8');
    return yaml.load(file);
}

// Fetch JSON schema from the provided URL
function fetchSchema(url, callback) {
    https.get(url, (resp) => {
        let data = '';

        // A chunk of data has been received.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Parse and return the result.
        resp.on('end', () => {
            callback(null, JSON.parse(data));
        });

    }).on("error", (err) => {
        callback(err, null);
    });
}

// Validate the YAML content against the provided schema
function validate(yamlContent, schema) {
    const ajv = new Ajv({ strict: false });
    const validate = ajv.compile(schema);

    const valid = validate(yamlContent);
    if (!valid) {
        console.error(validate.errors);
        process.exit(1);
    } else {
        console.log("Validation successful!");
        process.exit(0);
    }
}

// Example usage
if (process.argv.length < 4) {
    console.error("Usage: node validate.js <path_to_yaml_file> <schema_url>");
    process.exit(1);
}

const yamlFilePath = process.argv[2];
const schemaURL = process.argv[3];

const yamlContent = loadYAML(yamlFilePath);

fetchSchema(schemaURL, (err, schema) => {
    if (err) {
        console.error("Error fetching schema:", err);
        process.exit(1);
    } else {
        validate(yamlContent, schema);
    }
});
