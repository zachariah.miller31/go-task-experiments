module github.com/yourusername/yourprojectname

go 1.20

require gopkg.in/yaml.v2 v2.4.0

require (
	github.com/kr/pretty v0.3.1 // indirect
	github.com/rogpeppe/go-internal v1.10.1-0.20230524175051-ec119421bb97 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
