#!/bin/bash

# Comma delimited list of architectures
ARCH_LIST="amd64,arm64"

# Image name
IMAGE_NAME="builder-image"
TAG=latest
# Registry
#REGISTRY="ghcr.io/zachariahmiller/go-task-experiments"
REGISTRY="registry.gitlab.com/zachariah.miller31/go-task-experiments"

LISTNAME="$REGISTRY/$IMAGE_NAME:$TAG"
podman manifest create $LISTNAME
IFS=','
for ARCH in $ARCH_LIST; do
  IMAGE="$REGISTRY/$IMAGE_NAME:$TAG-$ARCH"
  # Build the image for the current architecture

  podman build --platform "linux/$ARCH" -t "$IMAGE" --manifest $LISTNAME .
  podman push "$IMAGE"
done

# Push the manifest list to the registry
podman manifest push "$REGISTRY/$IMAGE_NAME:$TAG"