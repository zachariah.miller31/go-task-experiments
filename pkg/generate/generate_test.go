package generate

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
)

const (
	gitlabSchemaUrl = "https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json"
	githubSchemaUrl = "https://raw.githubusercontent.com/SchemaStore/schemastore/master/src/schemas/json/github-workflow.json"
)

var (
	taskfilePath          = filepath.Join("..", "..", "Taskfile.yaml")
	gitlabTestFile        = filepath.Join("..", "..", "tests", ".gitlab-task-pipeline.yaml")
	githubTestFile        = filepath.Join("..", "..", "tests", "github_task_pipeline.yaml")
	invalidGitlabTestFile = filepath.Join("..", "..", "tests", "invalid-gitlab-task-pipeline.yaml")
	invalidGithubTestFile = filepath.Join("..", "..", "tests", "invalid-github-task-workflow.yaml")
	jsValidatorPath       = filepath.Join("..", "..", "hack", "js-validator")
	jsValidator           = filepath.Join(jsValidatorPath, "validate.js")
)

func ensureJSDependencies() error {
	cmd := exec.Command("npm", "install")
	cmd.Dir = jsValidatorPath // Change this to the path where package.json resides
	return cmd.Run()
}

func TestMain(m *testing.M) {
	// Setup: Ensure JS dependencies are installed
	err := ensureJSDependencies()
	if err != nil {
		// Log the error and exit
		// This will prevent any of the tests from running if setup fails
		// Adjust this behavior as necessary for your use case
		println("Failed to install JavaScript dependencies:", err)
		os.Exit(1)
	}

	// Run the tests
	exitVal := m.Run()

	// Teardown: Any cleanup after tests can go here if necessary

	os.Exit(exitVal)
}

func validateYAMLWithNode(yamlFilePath string, schemaURL string) error {
	cmd := exec.Command("node", jsValidator, yamlFilePath, schemaURL)
	output, err := cmd.CombinedOutput()
	if err != nil {
		return fmt.Errorf("Node validation failed: %s\nOutput: %s", err, output)
	}
	return nil
}

func TestGitLabCIYamlGeneration(t *testing.T) {

	absPath, _ := filepath.Abs(taskfilePath)
	t.Log("Reading Taskfile from:", absPath)
	if _, err := os.Stat(taskfilePath); os.IsNotExist(err) {
		t.Fatalf("Taskfile.yaml does not exist at path: %s", taskfilePath)
	} // Adjust path accordingly if your Taskfile's location changes
	tf, err := ReadTaskfile(taskfilePath)
	if err != nil {
		t.Fatalf("Failed to read Taskfile: %s", err)
	}

	if err := GenerateGitLabCI(tf, gitlabTestFile); err != nil {
		t.Fatalf("Failed to generate GitLab CI content: %s", err)
	}
	err = validateYAMLWithNode(gitlabTestFile, gitlabSchemaUrl)
	if err != nil {
		t.Fatal(err)
	} else {
		t.Logf("The yaml generated in %s is valid a gitlab pipeline based on the provided schema %s", gitlabTestFile, gitlabSchemaUrl)
	}
}

func TestGitHubActionsYamlGeneration(t *testing.T) {

	absPath, _ := filepath.Abs(taskfilePath)
	t.Log("Reading Taskfile from:", absPath)
	if _, err := os.Stat(taskfilePath); os.IsNotExist(err) {
		t.Fatalf("Taskfile.yaml does not exist at path: %s", taskfilePath)
	}

	tf, err := ReadTaskfile(taskfilePath)
	if err != nil {
		t.Fatalf("Failed to read Taskfile: %s", err)
	}

	if err := GenerateGitHubWorkflow(tf, githubTestFile); err != nil {
		t.Fatalf("Failed to generate github workflow content: %s", err)
	}
	err = validateYAMLWithNode(githubTestFile, githubSchemaUrl)
	if err != nil {
		t.Fatal(err)
	} else {
		t.Logf("The yaml generated in %s is valid a workflow based on the provided schema %s", githubTestFile, githubSchemaUrl)
	}

}

func TestInvalidGitLabYAMLFile(t *testing.T) {

	// Your schema validation logic here using invalidYamlFile
	err := validateYAMLWithNode(invalidGitlabTestFile, gitlabSchemaUrl)
	if err == nil {
		t.Fatalf("Expected invalid YAML file to fail validation, but it passed.")
	} else {
		t.Logf("The yaml in %s is an invalid gitlab pipeline yaml, as expected in this test, based on the provided schema %s.", invalidGitlabTestFile, gitlabSchemaUrl)
	}
}

func TestInvalidGitHubYAMLFile(t *testing.T) {

	// Your schema validation logic here using invalidYamlFile
	err := validateYAMLWithNode(invalidGithubTestFile, githubSchemaUrl)
	if err == nil {
		t.Fatalf("Expected invalid YAML file to fail validation, but it passed.")
	} else {
		t.Logf("The yaml in %s is an invalid gitlab pipeline yaml, as expected in this test, based on the provided schema %s.", invalidGithubTestFile, githubSchemaUrl)
	}
}
