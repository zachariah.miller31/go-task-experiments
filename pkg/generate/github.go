// github.go
package generate

import (
	"fmt"
	"os"
)

func GenerateGitHubWorkflow(tf *Taskfile, filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	// Write the header
	file.WriteString("name: Main Workflow\n\non: [push, pull_request]\n\njobs:\n")

	// Handle tasks
	for taskName, task := range tf.Tasks {
		if taskName == "default" {
			continue
		}

		image := ""
		if taskImage, exists := task.Vars["IMAGE_REF"]; exists {
			image = taskImage
		}

		// Write to file
		file.WriteString(fmt.Sprintf("  %s:\n", taskName))
		file.WriteString("    runs-on: ubuntu-latest\n")
		if image != "" {
			file.WriteString(fmt.Sprintf("    container: %s\n", image))
		}
		if len(task.Deps) > 0 {
			file.WriteString("    needs:\n")
			for _, dep := range task.Deps {
				file.WriteString(fmt.Sprintf("      - %s\n", dep))
			}
		}
		file.WriteString(fmt.Sprintf("    steps:\n      - name: Execute task %s\n        run: task %s\n\n", taskName, taskName))
	}

	return nil
}
