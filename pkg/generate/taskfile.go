// taskfile.go
package generate

import (
	"os"

	"gopkg.in/yaml.v2"
)

type Task struct {
	Vars map[string]string `yaml:"vars,omitempty"`
	Deps []string          `yaml:"deps"`
}

type Taskfile struct {
	Vars  map[string]string `yaml:"vars,omitempty"`
	Tasks map[string]*Task  `yaml:"tasks"`
}

func ReadTaskfile(filename string) (*Taskfile, error) {
	data, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	var tf Taskfile
	if err = yaml.Unmarshal(data, &tf); err != nil {
		return nil, err
	}

	return &tf, nil
}
