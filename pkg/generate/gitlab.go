// gitlab.go
package generate

import (
	"fmt"
	"os"
	"strings"
)

func GenerateGitLabCI(tf *Taskfile, filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	// Write the header
	file.WriteString("stages:\n  - build\n\n")

	// Handle global vars
	if globalImage, exists := tf.Vars["IMAGE_REF"]; exists {
		file.WriteString("default:\n")
		file.WriteString(fmt.Sprintf("  image: \"%s\"\n", globalImage))
		if tags, ok := tf.Vars["TAGS"]; ok {
			file.WriteString("  tags:\n")
			for _, tag := range strings.Split(tags, ",") {
				file.WriteString(fmt.Sprintf("    - %s\n", tag))
			}
		}
		file.WriteString("\n")
	}

	// Handle tasks
	for taskName, task := range tf.Tasks {
		if taskName == "default" {
			continue
		}

		// Determine image reference
		image := ""
		if taskImage, exists := task.Vars["IMAGE_REF"]; exists {
			image = taskImage
		}

		// Write to file
		file.WriteString(fmt.Sprintf("%s:\n", taskName))
		if image != "" {
			file.WriteString(fmt.Sprintf("  image: %s\n", image))
		}
		file.WriteString("  stage: build\n")
		file.WriteString(fmt.Sprintf("  script:\n    - task %s\n", taskName))
		if len(task.Deps) > 0 {
			file.WriteString("  needs:\n")
			for _, dep := range task.Deps {
				file.WriteString(fmt.Sprintf("    - %s\n", dep))
			}
		}
		file.WriteString("\n")
	}

	return nil
}
